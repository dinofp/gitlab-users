# gitlab-users


## Synopsis

Demo of examining and adding user to a Gitlab project


## Description

This is a small demo project to drive Gitlab's REST API to examine and modify
the users on a project.


## Getting source

Source code is available from gitlab at the [gitlab-users](https://gitlab.com/dinofp/gitlab-users.git) project page.


## Development

Run like this:

    $ GITLAB_TOKEN=<your_gitlab_token> PROJECT_NAME=<project_id> stack exec gitlab-users <gitlab_user_ids>


Sample output:

    $ GITLAB_TOKEN=REDACTED PROJECT_NAME=11620942 stack exec gitlab-users 3751905 3758604

    Users in this project:
    dinofp (id 3751905)

    Users missing:
    3758604

    Results of user add operation:
    3758604 add successful, name: dmfunctional

    $ GITLAB_TOKEN=REDACTED PROJECT_NAME=11620942 stack exec gitlab-users 3751905 3758604

    Users in this project:
    dinofp (id 3751905)
    dmfunctional (id 3758604)

    Users missing:

    All users present, nothing to add

    $


## Contact

Dino Morelli <dino@ui3.info>
