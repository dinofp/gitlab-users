{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Gitlab.Client
  ( Token (..)
  , addProjectMember
  , getProjectMembers
  )
  where

import Data.Proxy
import Network.HTTP.Client ( newManager )
import Network.HTTP.Client.TLS ( tlsManagerSettings )
import Servant.API
import Servant.Client

import Gitlab.Data.User ( GitlabUser )


newtype Token = Token String


type TokenHeader = Header "PRIVATE-TOKEN" String

type GetProjectMembers
  =  TokenHeader
  :> "projects" :> Capture "id" Int :> "members"
  :> Get '[JSON] [GitlabUser]

type AddProjectMember
  =  TokenHeader
  :> "projects" :> Capture "id" Int :> "members"
  :> QueryParam "user_id" Int :> QueryParam "access_level" Int
  :> Post '[JSON] GitlabUser

type API
  =    GetProjectMembers
  :<|> AddProjectMember


api :: Proxy API
api = Proxy


getProjectMembers' :: Maybe String -> Int -> ClientM [GitlabUser]
addProjectMember'  :: Maybe String -> Int -> Maybe Int -> Maybe Int -> ClientM GitlabUser

getProjectMembers' :<|> addProjectMember' = client api


baseUrl' :: BaseUrl
baseUrl' = BaseUrl Https "gitlab.com" 443 "/api/v4"


makeRequest :: ClientM a -> IO (Either ServantError a)
makeRequest queryFunc = do
  manager' <- newManager tlsManagerSettings
  runClientM queryFunc (mkClientEnv manager' baseUrl')


getProjectMembers :: Token -> Int -> IO (Either ServantError [GitlabUser])
getProjectMembers (Token tokenValue) projectId =
  makeRequest $ getProjectMembers' (Just tokenValue) projectId


accessDeveloper :: Int
accessDeveloper = 30


addProjectMember :: Token -> Int -> Int -> IO (Either ServantError GitlabUser)
addProjectMember (Token tokenValue) projectId userid =
  makeRequest $ addProjectMember' (Just tokenValue) projectId (Just userid) (Just accessDeveloper)
