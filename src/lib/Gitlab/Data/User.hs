{-# LANGUAGE DeriveGeneric #-}

module Gitlab.Data.User
  ( GitlabUser (..)
  , formatUser
  )
  where

import Data.Aeson
import Data.Text ( Text, unpack )
import GHC.Generics


{- Model of the GitLab user account data
[
    {
        "id": 1234567,
        "name": "Stimpson J Cat",
        "username": "stimpy",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/blahblahblah",
        "web_url": "https://gitlab.com/stimpy",
        "access_level": 40,
        "expires_at": null
    }
]
-}

-- data State = Active

-- data Access = Guest | Reporter | Developer | Maintainer | Owner
--   deriving Ord

data GitlabUser = GitlabUser
  { guId :: Int
  , guUsername :: Text
  -- , guName :: Text
  -- , guState :: State
  -- , guAvUrl :: Text
  -- , guWebUrl :: Text
  -- , guExpires :: Maybe UTCTime
  -- , guAccess :: Access
  }
  deriving (Generic, Show)

instance FromJSON GitlabUser where
  parseJSON (Object o) = GitlabUser
    <$> o .: "id"
    <*> o .: "username"
    -- <*> o .: "name"
    -- <*> o .: "state"
    -- <*> o .: "avatar_url"
    -- <*> o .: "web_url"
    -- <*> o .: "expires_at"
    -- <*> o .: "access_level"
  parseJSON o = fail . show $ o


formatUser :: GitlabUser -> String
formatUser (GitlabUser guId' guUsername') =
  unpack guUsername' ++ " (id " ++ show guId' ++ ")"
