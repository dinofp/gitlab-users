import Data.Text ( unpack )
import System.Environment ( getArgs, lookupEnv )

import Gitlab.Data.User ( GitlabUser (GitlabUser, guId), formatUser )
import Gitlab.Client ( Token (..), addProjectMember, getProjectMembers )


tokenVarName, projectIdVarName :: String
tokenVarName = "GITLAB_TOKEN"
projectIdVarName = "PROJECT_NAME"


main :: IO ()
main = do
  mbToken <- lookupEnv tokenVarName
  mbProjectId <- lookupEnv projectIdVarName
  userList <- map read <$> getArgs

  tokenValue <- maybe (error $ "No GitLab token set in " ++ tokenVarName) return mbToken
  projectId <- maybe (error $ "No GitLab project name set in " ++ projectIdVarName) (return . read) mbProjectId

  getResult <- getProjectMembers (Token tokenValue) projectId

  case getResult of
    Left err -> error $ "Error: " ++ show err
    Right usersInProject -> do
      listUsersInProject usersInProject
      let usersMissing = locateMissing usersInProject userList
      listMissingUsers usersMissing
      addResults <- mapM (addProjectMember (Token tokenValue) projectId) usersMissing
      displayAddResults $ zip usersMissing addResults


listUsersInProject :: [GitlabUser] -> IO ()
listUsersInProject users = do
  putStrLn "\nUsers in this project:"
  mapM_ (putStrLn . formatUser) users


locateMissing :: [GitlabUser] -> [Int] -> [Int]
locateMissing usersInProject = filter (not . flip elem useridsInProject)
  where
    useridsInProject = map guId usersInProject


listMissingUsers :: [Int] -> IO ()
listMissingUsers usersMissing = do
  putStrLn "\nUsers missing:"
  mapM_ print usersMissing


displayAddResults :: [(Int, Either e GitlabUser)] -> IO ()

displayAddResults [] =
  putStrLn "\nAll users present, nothing to add"

displayAddResults addResults = do
  putStrLn "\nResults of user add operation:"
  mapM_ (putStrLn . formatAddResults) addResults


formatAddResults :: (Int, Either e GitlabUser) -> String
formatAddResults (userid, Left _) = show userid ++ " add FAILED"
formatAddResults (_ , Right (GitlabUser guId' guUsername'))
  = show guId' ++ " add successful, name: " ++ unpack guUsername'
